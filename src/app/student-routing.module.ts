import { RouterModule, Routes} from '@angular/router';
import {StudentsAddComponent} from './students/add/students.add.component';
import {StudentsViewComponent} from './students/view/students.view.component';
import {StudentsComponent} from './students/list/students.component';
import {NgModule} from '@angular/core';


const StudentRoutes: Routes = [
    { path: 'view', component: StudentsViewComponent},
    { path: 'add', component: StudentsAddComponent},
    { path: 'list', component: StudentsComponent},
    { path: 'detail/:id', component: StudentsViewComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}