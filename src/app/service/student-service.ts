import { Observable } from '../../../node_modules/rxjs';
import Student  from '../entity/student';
import { StudentsFileImplService} from '../service/students-file-impl.service';

export abstract class StudentService {
     abstract getStudents(): Observable<Student[]>;
     abstract getStudent(id: number): Observable<Student>;
}
